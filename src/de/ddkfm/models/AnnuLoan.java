package de.ddkfm.models;

public class AnnuLoan extends Loan {

	public AnnuLoan(double credit, int runtime, double intrestRate) {
		super(credit, runtime, intrestRate);
	}

	@Override
	protected double getRemaining(int jear) {
		return(jear == 1)?credit:getRemaining(jear-1)- getRepayment(jear-1);
	}

	@Override
	protected double getIntrest(int jear) {
		return getRemaining(jear) * intrestRate;
	}

	@Override
	protected double getRepayment(int jear) {
		return getTotalBurden(jear) - getIntrest(jear);
	}

	@Override
	protected double getTotalBurden(int jear) {
		double p_exp = Math.pow(1+intrestRate,runtime);
        return credit * p_exp * intrestRate / (p_exp - 1);
	}

}
