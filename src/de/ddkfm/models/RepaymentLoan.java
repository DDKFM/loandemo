package de.ddkfm.models;

public class RepaymentLoan extends Loan {

	public RepaymentLoan(double credit, int runtime, double intrestRate) {
		super(credit, runtime, intrestRate);
	}

	@Override
	protected double getRemaining(int jear) {
		return credit - getRepayment(jear) * (jear - 1);
	}

	@Override
	protected double getIntrest(int jear) {
		return getRemaining(jear) * intrestRate;
	}

	@Override
	protected double getRepayment(int jear) {
		return credit / runtime;
	}

	@Override
	protected double getTotalBurden(int jear) {
		return getRepayment(jear) + getIntrest(jear);
	}

}
