package de.ddkfm.models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;

public abstract class Loan {
	protected double credit;
	protected int runtime;
	protected double intrestRate;

	public Loan(double credit, int runtime, double intrestRate) {
		super();
		this.credit = credit;
		this.runtime = runtime;
		this.intrestRate = intrestRate;
	}

	protected abstract double getRemaining(int jear);

	protected abstract double getIntrest(int jear);

	protected abstract double getRepayment(int jear);

	protected abstract double getTotalBurden(int jear);

	private static double round(double d) {
		return Math.round(d * 100d) / 100d;
	}

	public ObservableList<LoanLine> getObservableList() {
		ObservableList<LoanLine> data = FXCollections.observableArrayList();
		for (int i = 1; i <= runtime; i++) {
			data.add(new LoanLine(i, round(getIntrest(i)), round(getRemaining(i)), round(getRepayment(i)),
					round(getTotalBurden(i))));
		}
		return data;
	};

	public ObservableList<TotalLoanLine> getTotalList() {
		double totalIntrest = 0;
		for(int n = 1; n<=runtime ; n++)
			totalIntrest += getIntrest(n);
		return FXCollections.observableArrayList(new TotalLoanLine(round(totalIntrest), round(credit + totalIntrest)));
	}
	public XYChart.Series<Number, Number> getIntrestChart(){
		XYChart.Series<Number, Number> chart = new XYChart.Series<>();
		for(int i = 1;i<=runtime;i++)
			chart.getData().add(new XYChart.Data<>(i,getIntrest(i)));
		chart.setName("Zinsen");
		return chart;
	}
	public XYChart.Series<Number, Number> getRemainingChart(){
		XYChart.Series<Number, Number> chart = new XYChart.Series<>();
		for(int i = 1;i<=runtime;i++)
			chart.getData().add(new XYChart.Data<>(i,getRemaining(i)));
		chart.setName("Restschuld");
		return chart;
	}
	public XYChart.Series<Number, Number> getRepaymentChart(){
		XYChart.Series<Number, Number> chart = new XYChart.Series<>();
		for(int i = 1;i<=runtime;i++)
			chart.getData().add(new XYChart.Data<>(i,getRepayment(i)));
		chart.setName("Tilgung");
		return chart;
	}
	public XYChart.Series<Number, Number> getTotalBurdenChart(){
		XYChart.Series<Number, Number> chart = new XYChart.Series<>();
		for(int i = 1;i<=runtime;i++)
			chart.getData().add(new XYChart.Data<>(i,getTotalBurden(i)));
		chart.setName("Gesamtbelastung");
		return chart;
	}

}
