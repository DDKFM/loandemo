package de.ddkfm.models;

import javafx.beans.property.SimpleDoubleProperty;

public class TotalLoanLine {
	private SimpleDoubleProperty totalIntrest;
	private SimpleDoubleProperty totalBurden;
	
	public TotalLoanLine(double totalInterest, double totalBurden){
		this.totalIntrest = new SimpleDoubleProperty(totalInterest);
		this.totalBurden = new SimpleDoubleProperty(totalBurden);
	}
	public void setTotalIntrest(double totalIntrest){
		this.totalIntrest.set(totalIntrest);
	}
	public double getTotalIntrest(){
		return totalIntrest.get();
	}
	public void setTotalBurden(double totalBurden){
		this.totalBurden.set(totalBurden);
	}
	public double getTotalBurden(){
		return totalBurden.get();
	}
}
