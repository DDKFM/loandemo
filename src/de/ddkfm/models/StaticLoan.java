package de.ddkfm.models;

public class StaticLoan extends Loan {

	public StaticLoan(double credit, int runtime, double intrestRate) {
		super(credit, runtime, intrestRate);
	}

	@Override
	protected double getRemaining(int jear) {
		return credit;
	}

	@Override
	protected double getIntrest(int jear) {
		return credit * intrestRate;
	}

	@Override
	protected double getRepayment(int jear) {
		return (jear == runtime)?credit:0;
	}

	@Override
	protected double getTotalBurden(int jear) {
		return getRepayment(jear) + getIntrest(jear);
	}

}
