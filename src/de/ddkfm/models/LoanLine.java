package de.ddkfm.models;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class LoanLine {
	private SimpleIntegerProperty jear;
	private SimpleDoubleProperty intrest;
	private SimpleDoubleProperty remaining;
	private SimpleDoubleProperty repayment;
	private SimpleDoubleProperty totalBurden;
	public LoanLine(int jear, double intrest, double remaining, double repayment, double totalBurden) {
		this.jear = new SimpleIntegerProperty(jear);
		this.intrest = new SimpleDoubleProperty(intrest);
		this.remaining = new SimpleDoubleProperty(remaining);
		this.repayment = new SimpleDoubleProperty(repayment);
		this.totalBurden = new SimpleDoubleProperty(totalBurden);
	}
	public int getJear() {
		return jear.get();
	}
	public void setJear(int jear) {
		this.jear.set(jear);
	}
	public double getIntrest() {
		return intrest.get();
	}
	public void setIntrest(double intrest) {
		this.intrest.set(intrest);
	}
	public double getRemaining() {
		return remaining.get();
	}
	public void setRemaining(double remaining) {
		this.remaining.set(remaining);
	}
	public double getRepayment() {
		return repayment.get();
	}
	public void setRepayment(double repayment) {
		this.repayment.set(repayment);
	}
	public double getTotalBurden() {
		return totalBurden.get();
	}
	public void setTotalBurden(double totalBurden) {
		this.totalBurden.set(totalBurden);
	}
	
	
	
}
