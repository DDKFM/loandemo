package de.ddkfm.application;

import java.net.URL;
import java.util.ResourceBundle;

import de.ddkfm.models.AnnuLoan;
import de.ddkfm.models.Loan;
import de.ddkfm.models.LoanLine;
import de.ddkfm.models.RepaymentLoan;
import de.ddkfm.models.StaticLoan;
import de.ddkfm.models.TotalLoanLine;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

public class LoanDemoController implements Initializable {

	@FXML
	private Pane root;
	@FXML
	private RadioButton rbTilgungsdarlehen;

	@FXML
	private RadioButton rbFestdarlehen;

	@FXML
	private TableView<TotalLoanLine> tableTotalNumbers;

	@FXML
	private TableColumn<TotalLoanLine, Double> tcTotalIntrest;

	@FXML
	private TableColumn<TotalLoanLine, Double> tcAllTotalBurden;
	@FXML
	private TextField intrest;

	@FXML
	private ToggleGroup tgType;

	@FXML
	private TextField runtime;

	@FXML
	private TextField credit;

	private LineChart<Number, Number> chart;

	@FXML
	private TableView<LoanLine> tableAllNumbers;
	@FXML
	private TableColumn<LoanLine, Integer> tcJear;
	@FXML
	private TableColumn<LoanLine, Double> tcIntrest;
	@FXML
	private TableColumn<LoanLine, Double> tcRemaining;
	@FXML
	private TableColumn<LoanLine, Double> tcTotalBurden;
	@FXML
	private TableColumn<LoanLine, Double> tcRepayment;

	@FXML
	private RadioButton rbAnnuitaetendarlehen;

	@FXML
	private Button btCompute;

	@FXML
	void btComputeAction(ActionEvent event) {
		Loan loan;
		double aCredit = 0;
		Alert al = new Alert(AlertType.ERROR);
		al.setContentText("Sie haben folgende Daten falsch eingegeben\n");
		try {
			aCredit = Double.parseDouble(credit.getText().replace(',', '.'));
		} catch (NumberFormatException e) {
			al.setContentText(al.getContentText() + "   - Kreditsumme\n");
		}
		int aRuntime = 0;
		try {
			aRuntime = Integer.parseInt(runtime.getText());
		} catch (NumberFormatException e) {
			al.setContentText(al.getContentText() + "   - Lautzeit\n");
		}
		double aIntrestRate = 0;
		try {
			aIntrestRate = Double.parseDouble(intrest.getText().replace(',', '.')) / 100d;
		} catch (NumberFormatException e) {
			al.setContentText(al.getContentText() + "   - Zinssatz\n");
		}
		if (al.getContentText().equals("Sie haben folgende Daten falsch eingegeben\n")) {
			if (rbFestdarlehen.isSelected())
				loan = new StaticLoan(aCredit, aRuntime, aIntrestRate);
			else if (rbTilgungsdarlehen.isSelected())
				loan = new RepaymentLoan(aCredit, aRuntime, aIntrestRate);
			else
				loan = new AnnuLoan(aCredit, aRuntime, aIntrestRate);
			tableAllNumbers.setItems(loan.getObservableList());
			tableTotalNumbers.setItems(loan.getTotalList());
			chart.getData().clear();
			chart.getData().add(loan.getRepaymentChart());
			chart.getData().add(loan.getIntrestChart());
			chart.getData().add(loan.getTotalBurdenChart());
		} else {
			al.setContentText(al.getContentText() + " bitte pr�fen Sie ihre Eingaben");
			al.show();
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		tcJear.setCellValueFactory(new PropertyValueFactory<LoanLine, Integer>("jear"));
		tcRepayment.setCellValueFactory(new PropertyValueFactory<LoanLine, Double>("repayment"));
		tcRemaining.setCellValueFactory(new PropertyValueFactory<LoanLine, Double>("remaining"));
		tcIntrest.setCellValueFactory(new PropertyValueFactory<LoanLine, Double>("intrest"));
		tcTotalBurden.setCellValueFactory(new PropertyValueFactory<LoanLine, Double>("totalBurden"));
		
		tcTotalIntrest.setCellValueFactory(new PropertyValueFactory<>("totalIntrest"));
		tcAllTotalBurden.setCellValueFactory(new PropertyValueFactory<>("totalBurden"));
		
		final NumberAxis xAxis = new NumberAxis();
		xAxis.setLabel("Jahre");
		final NumberAxis yAxis = new NumberAxis();
		yAxis.setLabel("Betrag in �");
		chart = new LineChart<Number, Number>(xAxis,yAxis);
		root.getChildren().add(chart);
		chart.setLayoutX(468);
		chart.setLayoutY(16);
		chart.setPrefWidth(450);
		chart.setPrefHeight(580);
	}

}
